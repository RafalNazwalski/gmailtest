package com.nazw.tests;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.nazw.pageObject.GmailPageObject;

public class GmailTest {

	private final String login = "testjan475";
	private final String password = "typicalPassword";
	
	WebDriver driver;
	GmailPageObject gmailPageObject;
	
	@Before
	public void beforeGmailTest(){
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		gmailPageObject = new GmailPageObject(driver);
		
	}
	
	@Test
	public void gmailTest() {
		
		driver.get("http://www.gmail.com");
		gmailPageObject.typeLoginInput(login);
		gmailPageObject.clickDalejButton();
		gmailPageObject.typePasswordInput(password);
		gmailPageObject.clickZalogujSieButton();
		
		assertTrue(gmailPageObject.IsCorrectUserLogged(login));
	}

	@After
	public void afterGmailTest(){
		
		driver.quit();
		
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
