package com.nazw.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailPageObject extends BasePageObject {

	private final int timeoutInSeconds = 10;
	
	WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);

	public GmailPageObject(WebDriver driver) {
		super(driver);
	}

	@FindBy(id = "Email")
	private WebElement loginInput;

	@FindBy(xpath = "//*[contains(@value,'Dalej')]")
	private WebElement dalejButton;

	@FindBy(id = "Passwd")
	private WebElement passwordInput;

	@FindBy(id = "signIn")
	private WebElement zalogujSieButton;

	@FindBy(xpath = "//*[contains(@title, 'gmail')]")
	private WebElement accountName;

	public void typeLoginInput(String login) {
		loginInput.sendKeys(login);
	}

	public void clickDalejButton() {
		dalejButton.click();
	}

	public void typePasswordInput(String password) {
		wait.until(ExpectedConditions.elementToBeClickable(passwordInput));
		passwordInput.sendKeys(password);
	}

	public void clickZalogujSieButton() {
		zalogujSieButton.click();
	}

	public boolean IsCorrectUserLogged(String accountName) {
		wait.until(ExpectedConditions.elementToBeClickable(this.accountName));

		System.out.println("Element from page: " + this.accountName.getAttribute("title"));
		System.out.println("Element expected: " + accountName);

		if (this.accountName.getAttribute("title").contains(accountName)) {
			return true;
		} else {
			return false;
		}
	}

}
